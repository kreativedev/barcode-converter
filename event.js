// callback function
function onItemClick(info, tab){
	// Inject the content script into the current page
	chrome.tabs.executeScript(null, { file: 'content.js' });
}

// Perform the callback when a message is received from the content script
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){

	if (request.action == 'submit the form'){
		//message should contain the selected text and url - ensure that this is the correct message
		var url = "data:text/html;charset=utf8,";
		
		
		function append(key, value){
			var input = document.createElement('textarea');
			input.setAttribute('name', key);
			input.textContent = value;
			form.appendChild(input);
		}
		
		var form = document.createElement('form');
		// form.method = 'POST';
		// form.action = 'http://localhost:8000/barcode/';
		// form.style.visibility = "hidden";
		append('url', request.url);
		append('text', request.selectedText);
		// url = url + encodeURIComponent(form.outerHTML);
		// url = url + encodeURIComponent('<script>document.forms[0].submit();</script>');
		getSku(request, sender, sendResponse, request.selectedText)
		// chrome.tabs.create({url: url, active: true});
		return true;
	}
});

function getSku(request, sender, sendResponse, selection){
	var resp = sendResponse;

	$.ajax({
		url: "http://localhost:8000/barcode/",
		method: "POST",
		data: {"text": selection},
		dataType: 'json',
		success: function (retval) {
			if(retval.errors != ""){
				alert(retval.errors);
			}
			else{
				console.log("Matched barcode " + selection + " to " + retval.result);
			}
		}
	});
}

var context = "selection";
var title = "Convert %s to SKU";
var id = chrome.contextMenus.create({"title": title, "contexts": [context], "onclick": onItemClick});